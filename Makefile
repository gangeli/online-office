
.DEFAULT_GOAL := compile

compile:
	yarn build

watch:
	yarn start

check:
	CI=true yarn test

deploy:
	yarn build
	gsutil -m \
		-h "Cache-Control:no-cache" \
		rsync -d -r build/ gs://conversation.team/
	gsutil -m \
		-h "Cache-Control:no-cache" \
		rsync -d -r build/ gs://pseudoffice.com/
	gsutil -m \
		-h "Cache-Control:no-cache" \
		rsync -d -r build/ gs://pseudooffice.com/
	gsutil -m \
		-h "Cache-Control:no-cache" \
		rsync -d -r build/ gs://weworkonline.us/
	gsutil -m \
		-h "Cache-Control:no-cache" \
		rsync -d -r build/ gs://theonlineoffice.us/
