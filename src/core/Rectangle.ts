/**
 * A rectangle, defined by it's top left corner and its dimensions. Nothing fancy here.
 */
export default class Rectangle {
  top: number;
  left: number;
  width: number;
  height: number;

  constructor(top: number, left: number, width: number, height: number) {
    this.top = top;
    this.left = left;
    this.width = width;
    this.height = height;
  }

   overlaps = (other: Rectangle): boolean => {
    const aLeftOfB = this.left + this.width <= other.left;
    const aRightOfB = this.left >= other.left + other.width;
    const aAboveB = this.top >= other.top + other.height;
    const aBelowB = this.top + this.height <= other.top;

    return !( aLeftOfB || aRightOfB || aAboveB || aBelowB );
  };

  overlapsAny = (others: Set<Rectangle>): boolean => {
    let anyOverlap = false;
    others.forEach((other: Rectangle) => {
      anyOverlap = anyOverlap || this.overlaps(other);
    });
    return anyOverlap;
  };

}