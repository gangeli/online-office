import SharedValue from './SharedValue';

function mkSharedValuePair(initialValue?: string): {local: SharedValue<string>, remote: SharedValue<string>} {
  const remote: Array<SharedValue<string>> = [];
  const local: SharedValue<string> = new SharedValue(
    (value: string, version: number) => setTimeout(() => remote[0].receiveValue(value, version), 1),
    (version: number) => setTimeout(() => remote[0].commit(version), 1),
    initialValue
  );
  remote.push(new SharedValue(
    (value: string, version: number) => setTimeout(() => local.receiveValue(value, version), 1),
    (version: number) => setTimeout(() => local.commit(version), 1),
  ));
  return {local, remote: remote[0]};
}

describe('SharedValue', () => {
  test('has a working harness', async () => {
    mkSharedValuePair();
  });

  test('starts without consensus', async () => {
    const {local, remote} = mkSharedValuePair();
    expect(local.haveConsensus).toBeFalsy();
    expect(local.value).toBeNull();
    expect(remote.haveConsensus).toBeFalsy();
    expect(remote.value).toBeNull();
  });

  test('starts without consensus even with an initial value', async () => {
    const {local, remote} = mkSharedValuePair('initial');
    expect(local.haveConsensus).toBeFalsy();
    expect(local.value).toBeNull();
    expect(remote.haveConsensus).toBeFalsy();
    expect(remote.value).toBeNull();
  });

  test('setting a value does not immediately generate consensus', async () => {
    const {local, remote} = mkSharedValuePair();
    local.set('initial');
    expect(local.haveConsensus).toBeFalsy();
    expect(remote.haveConsensus).toBeFalsy();
  });

  test('setting a value eventually reaches consensus', async () => {
    const {local, remote} = mkSharedValuePair();
    await local.set('initial');
    expect(local.haveConsensus).toBeTruthy();
    expect(remote.haveConsensus).toBeTruthy();
    expect(local.value).toBe('initial');
    expect(remote.value).toBe('initial');
  });

  test('local value leads remote value', async () => {
    const {local, remote} = mkSharedValuePair();
    const promise = local.set('initial');
    expect(local.localValue).toBe('initial');
    expect(local.value).toBeNull();
    expect(remote.localValue).toBeNull();
    await promise;
    expect(local.value).toBe('initial');
    expect(remote.value).toBe('initial');
  });

  test('only the latest set value reaches consensus', async () => {
    const {local, remote} = mkSharedValuePair();
    const initialPromise = local.set('initial');
    expect(initialPromise).rejects.toThrowError(new Error('A newer value has been set on this SharedValue'));
    await local.set('overwrite');
    expect(local.haveConsensus).toBeTruthy();
    expect(remote.haveConsensus).toBeTruthy();
    expect(local.value).toBe('overwrite');
    expect(remote.value).toBe('overwrite');
  });

})